package com.example.jaxrs.jersey.springboot.rest;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

/**
 * Root resource (exposed at "myresource" path)
 */
//@Component
@Path("myresource")
public class MyResource {

    /**
     * Method handling HTTP GET requests. The returned object will be sent
     * to the client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     */
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getIt() {
        return "Got it!";
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getItJson() {
        return "\"Got it!\"";
    }

    @Path("array")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String[] getArrayJson() {
        return new String[]{"Hello", "World"};
    }

}
