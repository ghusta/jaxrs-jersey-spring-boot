package com.example.jaxrs.jersey.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JaxrsJerseySpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(JaxrsJerseySpringBootApplication.class, args);
	}

}
