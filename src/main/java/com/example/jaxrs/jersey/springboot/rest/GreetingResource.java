package com.example.jaxrs.jersey.springboot.rest;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;

import java.net.URI;

@Path("greeting")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class GreetingResource {

    @GET
    public Greeting getHelloParam(@QueryParam("name") @DefaultValue("John Doe") String name) {
        return new Greeting(2, name);
    }

    @POST
    public Response insertGreeting(Greeting greeting, @Context UriInfo uriInfo) {
        System.out.printf("%d - %s%n", greeting.id(), greeting.content());
        System.out.println(uriInfo.getAbsolutePath());
        System.out.println(uriInfo.getBaseUri());
        URI uriTest = uriInfo.getAbsolutePathBuilder().path("123").build();
        System.out.println("TEST : " + uriTest.toString());
        return Response.created(uriTest).build();
    }

}
