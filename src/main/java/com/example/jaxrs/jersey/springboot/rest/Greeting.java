package com.example.jaxrs.jersey.springboot.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName
public record Greeting(@JsonProperty long id, String content) {

}
