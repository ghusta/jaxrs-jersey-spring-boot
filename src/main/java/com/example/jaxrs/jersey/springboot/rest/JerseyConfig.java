package com.example.jaxrs.jersey.springboot.rest;

import jakarta.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

/**
 * Specifique Jersey.
 *
 * @see org.springframework.boot.autoconfigure.jersey.JerseyAutoConfiguration
 */
@Configuration
@ApplicationPath("/api")
public class JerseyConfig extends ResourceConfig {

    private static final Logger log = LoggerFactory.getLogger(JerseyConfig.class);

    public JerseyConfig() {
        log.debug("Init config : JerseyConfig");

        // fastidieux !
        // register(MyResource.class);

        // scan packages
        Package currentPkg = JerseyConfig.class.getPackage();
        packages(true, currentPkg.getName());
    }

}
