= Test JAX-RS 3 + Jersey 3 + Spring Boot 3.0
:author: Guillaume Husta
:icons: font
:sectnums:
:toc: auto
:spring-boot-version: 3.0.0

== Init du projet

Voir : https://start.spring.io/[Spring Initializr]

Version de https://spring.io/projects/spring-boot[Spring Boot] utilisée : *DERNIERE* +
=> Utilise Jersey 3.1 +
=> Utilise JAX-RS 3.1

=== Génération ZIP du projet

https://start.spring.io/starter.zip?type=maven-project&language=java&baseDir=demo&groupId=com.example&artifactId=jersey-demo-spring-boot&name=jersey-demo-spring-boot&description=Demo+project+for+Spring+Boot&packageName=com.example.demo&packaging=jar&javaVersion=1.8style=jersey

== Lancement du serveur

[source]
----
mvn spring-boot:run
----

== Test API

Sur serveur : http://localhost:8380/api/

=== Clients REST pour tester

* https://restlet.com/modules/client/[Restlet Client] (extension Chrome)
* https://www.getpostman.com/[Postman] / https://postwoman.io/[Postwoman]
* CLI :
** https://curl.haxx.se/[curl]
** https://httpie.org/[HTTPie]

== Description API

https://jersey.github.io/documentation/latest/wadl.html[WADL] : http://localhost:8380/api/application.wadl

== Créer une image Docker

Voir guide : https://spring.io/guides/topicals/spring-boot-docker/[Spring Boot Docker]

* https://spring.io/guides/topicals/spring-boot-docker/#_a_basic_dockerfile[A Basic Dockerfile]
* https://spring.io/guides/topicals/spring-boot-docker/#_multi_stage_build[Multi-Stage Build]

Voir aussi _Optimisation du build Docker_ avec https://docs.docker.com/develop/develop-images/multistage-build/[Use multi-stage builds].

== Documentation

=== Spring Boot

* https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#web.servlet.jersey[JAX-RS and Jersey]

_Generated with https://asciidoctor.org/[Asciidoctor] {asciidoctor-version}_
